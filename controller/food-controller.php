<?php

  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  $action = $_REQUEST['action'];
  $conn = null;

  switch( $action ) {

    case 'cargar_supers':
      getMarkets();
      break;
    case 'cargar_productos':
      getProducts();
      break;
    case 'cargar_problemas':
      getDisseases();
      break;
    case 'buscar_comida':
      buscarComida();
      break;
  }
  function getMarkets(){

    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM supermarkets");
    $stmt->execute();

    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }
  function getProducts(){

    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM product");
    $stmt->execute();

    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }
  function getDisseases(){
    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM alimentary_disorder");
    $stmt->execute();

    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }
  function buscarComida(){
    $datos = $_REQUEST['datos']['disseases'] ?? null;
    $not = "";
    if($datos != null){
      $not=crearNOT($datos);
    }
    $query = $_REQUEST['datos']['food'];
    $array = array();
    $finalArray = array();

    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM product where nombre LIKE '%$query%' AND $not");
    $stmt->execute();
    $array[$query] = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $explode = explode(" ", $query);
    for($i = 0; $i < count($explode); $i++){
      include("access-data.php");
      $value = $explode[$i];
      $sql = "SELECT * FROM product where nombre LIKE '%$value%' AND $not";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $array[$value] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    foreach($array as $value => $key){
      $finalArray = array_merge($finalArray, $key);
    }
    $finalArray = array_unique($finalArray,SORT_REGULAR);
    echo json_encode($finalArray,JSON_FORCE_OBJECT);
  }
  function crearNOT($datos){
    $return = "";
    foreach ($datos as $key => $value) {
      if($value == "true"){
        $return .= "NOT alergenos='".$key."' AND ";
      }
    }
    return substr($return,0,-4);
  }
?>
