<?php

  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  $action = $_REQUEST['action'];
  $conn = null;

  switch( $action ) {

    case 'add_by_table':
      addByTable();
      break;
    case 'actualizar_tabla':
      actualizar_tabla();
      break;

  }
  function addByTable(){
    $query = buildQuery();
    $sql = "INSERT INTO " . $_REQUEST['datos']['table'] . " " . $query;
    include("access-data.php");
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    echo $conn->lastInsertId();
  }

  function buildQuery(){
    $data = $_REQUEST['datos']['tablas'];
    $query = "(`id`, `";
    $values = "('null', '";
    foreach ($data as $key => $value){
      $query .= $key."`, `";
      $values .= $value."', '";
    }
    return substr($query, 0, -3).") VALUES ".substr($values, 0, -3).")";
  }

  function actualizar_tabla(){
    $datos = $_REQUEST['datos']['a_actualizar'];
    $id = $_REQUEST['datos']['a_actualizar']['id'];
    $tabla = $_REQUEST['datos']['tabla'];
    $sql = "UPDATE $tabla SET ";
    foreach ($datos as $key => $value) {
      if($key != 'id'){
        $sql.= "$key = '$value', ";
      }
    }
    $sql = substr($sql,0,-2);
    $sql.= " WHERE id=$id";

    include("access-data.php");
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    echo $stmt->rowCount();
  }
?>
