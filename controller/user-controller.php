<?php

  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  $action = $_REQUEST['action'];
  $conn = null;

  switch( $action ) {

    case 'buscar_usuario':
      buscarUsuario();
      break;

    case 'registrar_usuario':
      registrarUsuario();
      break;

    case 'confirmar_correo':
      confirmarCorreo();
      break;

    case 'conectar_usuario':
      conectarUsuario();
      break;

    case 'logout':
      desconectarUsuario();
      break;

    case 'cargar_chats':
      cargarChats();
      break;

    case 'buscar_mas_chats':
      buscarMasChats();
      break;

    case 'specific_chat':
      loadSpecificChat();
      break;

    case 'send_message':
      sendMessage();
      break;

    case 'cargar_nuevos_mensajes':
      cargarNuevosMensajes();
      break;

    case 'cargar_recientes':
      cargarRecientes();
      break;

    case 'cargar_usuarios':
      cargarUsuarios();
      break;

    case 'sleep_chat':
      sleepChat();
      break;

    case 'eliminar_usuario':
      eliminarUsuario();
      break;

    case 'banear_usuario':
      banearUsuario();
      break;

    case 'cargar_tickets':
      getTickets();
      break;

    case 'cargar_sugerencias':
      getSuggestions();
      break;

    case 'update_profile':
      updateProfile();
      break;

    case 'enviar_sugerencia':
      enviarSugerencia();
      break;

    case 'enviar_ticket':
      enviarTicket();
      break;
  }
  function conectarUsuario(){

    $user = $_REQUEST['datos']['user'];
    $pass = $_REQUEST['datos']['pass'];

    include("access-data.php");
    $stmt = $conn->prepare("SELECT id, password, nombre, apellido, email, user, reg_date, confirmed, rol, visible FROM user WHERE user='$user'");
    $stmt->execute();
    $value = $stmt->fetch(PDO::FETCH_ASSOC);

    if($value){

      if( password_verify($pass, $value['password']) ){
        if( $value['confirmed'] && $value['visible'] != 0){
          $value['log'] = true;
          setcookie("SessionCookie", json_encode($value), time() + 3600, "/");
          echo true;
        } else if ($value['visible'] == 0){
          echo -2;
        } else {
          echo -1;
        }
      } else{
        echo -4;
      }
    } else {
      echo -3;
    }
  }

  function registrarUsuario(){

    $pass  = $_REQUEST['datos']['pass'];
    $user  = $_REQUEST['datos']['user'];
    $email = $_REQUEST['datos']['email'];

    if(!checkuser($user) && !checkmail($email)){

      $options = ['cost' => 8,];
      $hash = password_hash($pass, PASSWORD_BCRYPT, $options);
      include("access-data.php");
      $sql = "INSERT INTO user (id, email, user, password, reg_date, last_seen, confirmed, active, rol, visible) VALUES (NULL, '$email', '$user', '$hash', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0', '0', '0', '1');";
      $conn->exec($sql);
      $id = $conn->lastInsertId();

      enviarCorreo($email, $user, $id);
      echo 1;

    } else {

      echo -1;

    }
  }

  function enviarCorreo($email, $user, $id){
    $hash = md5( rand(0,1000) );
    $link = 'http://www.canyoueat-it.com/verify.php?email='.$email.'&hash='.$hash;

    insertarCorreoDB($email, $hash, $id);

    $to      = $email;
    $subject = 'Verificación de email';
    $message = '
      <html>
        <head>
          <title> Email de verificación de canyoueat-it</title>
        </head>
        <body>
          <p>¡Gracias por registrarte!<br> Este es el Usuario con el que te has registrado:</p>
          <p>------------------------<br>
          Usuario: '.$user.' <br>
          ------------------------</p>

          <p>Por favor, haz click al enlace que hay a continuación para verificar tu cuenta.</p>
          <a href="'.$link.'">'.$link.'</a>
        </body>
      </html>
    ';

    $headers = 'From:noreply@canyoueat-it.com'."\r\n";
    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    mail($to, $subject, $message, $headers);

  }

  function confirmarCorreo(){
    $email = $_REQUEST['datos']['email'];
    $hash  = $_REQUEST['datos']['hash'];
    include("access-data.php");
    $stmt = $conn->prepare("UPDATE user_activation_link, user SET used=1, confirmed = 1 where user_email='$email'");
    $stmt->execute();
    echo $stmt->rowCount();
  }

  function desconectarUsuario(){
    setcookie("SessionCookie", true, time() + 3600, "/");
  }

  function buscarMasChats(){
    $name = $_REQUEST['name'];

    include("access-data.php");
    $stmt = $conn->prepare("SELECT id, user FROM user WHERE user LIKE '%$name%'");
    $stmt->execute();
    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function loadSpecificChat(){
    $userid = $_REQUEST['userid'];
    $contactid = $_REQUEST['contactid'];

    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM user_messages WHERE (sent_by_user_id = $userid AND received_by_user_id = $contactid) OR (sent_by_user_id = $contactid AND received_by_user_id = $userid)");
    $stmt->execute();
    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function sendMessage(){
    $userid = $_REQUEST['userid'];
    $contactid = $_REQUEST['contactid'];
    $message = $_REQUEST['message'];

    // $message = htmlspecialchars($message,  ENT_QUOTES);
    $message = chineseToUnicode($message);

    include("access-data.php");
    $stmt = $conn->prepare("INSERT INTO user_messages (id, sent_by_user_id, received_by_user_id, message, time) VALUES (null, '$userid', '$contactid', '$message', CURRENT_TIMESTAMP)");
    $stmt->execute();
    $id = $conn->lastInsertId();

    $stmt = $conn->prepare("SELECT time, message from user_messages where id = '$id'");
    $stmt->execute();
    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function cargarNuevosMensajes(){
    $userid = $_REQUEST['userid'];
    $contactid = $_REQUEST['contactid'];
    $top = $_REQUEST['top'];

    include("access-data.php");
    $stmt = $conn->prepare("SELECT * from ( select * from user_messages  WHERE (received_by_user_id = $contactid AND sent_by_user_id = $userid) OR (sent_by_user_id = $contactid AND received_by_user_id = $userid) order by time DESC limit $top ) as T ORDER BY time ASC");
    $stmt->execute();
    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function cargarRecientes(){
    $userid = $_REQUEST['userid'];

    include("access-data.php");
    $stmt = $conn->prepare("select distinct sent_by_user_id, received_by_user_id, message, user.user, time from user_messages INNER JOIN user ON received_by_user_id = user.id where sent_by_user_id  = '$userid' OR received_by_user_id = '$userid' group by received_by_user_id order by time DESC");
    $stmt->execute();
    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function cargarUsuarios(){
    $rol = $_REQUEST['datos']['rol'];
    if($rol > 0){
      include("access-data.php");
      $stmt = $conn->prepare("SELECT id,	user,	email,	confirmed,	active,	reg_date,	last_seen, active,	rol,	visible from user order by id ");
      $stmt->execute();
      echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
    }
  }

  function sleepChat(){
    $userid = $_REQUEST['userid'];
    $contactid = $_REQUEST['contactid'];
    $totalMensajes = $_REQUEST['totalMensajes'];
    $differences = false;
    $totalSleep = 0;

    include("access-data.php");
    $stmt = $conn->prepare("SELECT count(*) from ( select * from user_messages  WHERE (received_by_user_id = $contactid AND sent_by_user_id = $userid) OR (sent_by_user_id = $contactid AND received_by_user_id = $userid) order by time DESC) as T ORDER BY time ASC");

    while(!$differences){
      sleep(5);

      $stmt->execute();
      $json = $stmt->fetch(PDO::FETCH_ASSOC);

      if($totalMensajes < $json['count(*)'] || $totalSleep > 20 ){
        $differences = true;
      }

      $totalSleep = $totalSleep + 5;
    }

    if($totalSleep > 20){
      echo null;
    } else {
      $total = $json['count(*)'] - $totalMensajes;
      $stmt = $conn->prepare("SELECT * from ( select * from user_messages  WHERE (received_by_user_id = $contactid AND sent_by_user_id = $userid) OR (sent_by_user_id = $contactid AND received_by_user_id = $userid) order by time DESC limit $total) as T ORDER BY time ASC");
      $stmt->execute();
      $json = json_encode($stmt->fetch(PDO::FETCH_ASSOC));

      echo $json;
    }

  }

  function eliminarUsuario(){
    $id = $_REQUEST['datos']['id'];
    include("access-data.php");
    $stmt = $conn->prepare("UPDATE user SET visible='0' WHERE id=$id");
    $stmt->execute();
    echo "done";
  }

  function banearUsuario(){

  }

  function insertarCorreoDB($email, $hash, $id){
      include("access-data.php");
      $sql = "INSERT INTO user_activation_link (id, hash, user_id, user_email) VALUES (NULL, '$hash', '$id', '$email');";
      $conn->exec($sql);
  }

  function getTickets(){
    include("access-data.php");
    $stmt = $conn->prepare("SELECT tickets.* FROM tickets INNER JOIN user ON tickets.enviado_por = user.id");
    $stmt->execute();

    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function getSuggestions(){
    include("access-data.php");
    $stmt = $conn->prepare("SELECT * FROM suggestion");
    $stmt->execute();

    echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
  }

  function updateProfile(){
    $datos = $_REQUEST['datos'];
    $nombre=$datos['nombre'] ?? 'null';
    $apellido=$datos['apellido'] ?? 'null';
    $id=$datos['id'];
    $hash = "";

    if(isset($datos['password'])){
      $userpass=$_REQUEST['datos']['password'];
      $options = ['cost' => 8,];
      $hash = password_hash($userpass, PASSWORD_BCRYPT, $options);
      $hash = ", password = '$hash'";
    }
    $sql = "UPDATE user SET nombre='$nombre', apellido='$apellido' $hash where id=$id";

    include("access-data.php");
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    echo $stmt->rowCount();
  }

  function enviarSugerencia(){
    $mensaje = $_REQUEST['datos']['mensaje'];
    include("access-data.php");
    $sql ="INSERT INTO suggestion (id, sugerencia, fecha, estado) VALUES (null, '$mensaje', CURRENT_TIMESTAMP, 0)";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    echo $conn->lastInsertId();
  }
  function enviarTicket(){
    echo var_dump($_REQUEST);
    $mensaje = $_REQUEST['datos']['mensaje'];
    $id = $_REQUEST['datos']['id'];
    include("access-data.php");
    $sql ="INSERT INTO tickets (id, problema, enviado_por, estado, respuesta, fecha) VALUES (null, '$mensaje', '$id', 1, 'Por responder', CURRENT_TIMESTAMP)";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    echo $conn->lastInsertId();
  }

  function checkmail($email){
    include("access-data.php");
    $stmt = $conn->prepare("SELECT email FROM user where email='$email'");
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $value = $stmt->fetch();
    return $value;
  }

  function checkuser($user){
    include("access-data.php");
    $stmt = $conn->prepare("SELECT user FROM user where user='$user'");
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $value = $stmt->fetch();
    return $value;
  }

  function chineseToUnicode($str){
    preg_match_all('/./u',$str,$matches);
    $c = "";
    foreach($matches[0] as $m){
      $c .= "&#".base_convert(bin2hex(iconv('UTF-8',"UCS-4",$m)),16,10);
    }
    return $c;
  }
?>
