<html>
<?php
  if(!isset($_COOKIE['idioma'])){
    setCookie('idioma','ES');
  }
  include_once('session/sessions.php');
?>
  <head>

    <?php include_once('includes/meta.html');?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link href="assets/css/style.min.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/main.css'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
  </head>
  <body>
    <?php include_once('includes/navbar.php'); ?>
    <div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-3 mt-3">
            <div class="input-group">
              <input type="text" class="form-control food-p" placeholder="¿Qué problemas alimentarios tienes?" aria-label="" aria-describedby="basic-addon1">
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group mb-3 mt-3">
              <input id="food-to-search" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
              <div class="input-group-append">
                <button class="btn btn-info search-button" type="button">Busca!</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table id="tabla-index" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th >Nombre</th>
                  <th >Composición</th>
                  <th >Alergenos</th>
                </tr>
              </thead>
              <tbody>
                <tr class="relleno-prueba">
                  <td>Todavía</td>
                  <td>No hay</td>
                  <td>Busquedas</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <?php if(isset($_SESSION['user']['log'])){ ?>
      <div class="toast" data-autohide="false">
        <div class="toast-header">
          <strong class="mr-auto text-primary">Sugerencia</strong>
          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
        </div>
        <div class="toast-body">
          ¿Quieres enviar una sugerencia? Haz click <a class="toast_sugerencia" href="">aquí</a> para enviarla.
        </div>
      </div>
    <?php } ?>
    <footer class="page-footer">
      <div class="footer-copyright text-center py-3">2019
        <a href="https://gitlab.com/antigordas/newcanyoueat-it"> Anturiuso Project</a>
      </div>
    </footer>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/js.js'></script>
  <script src='assets/js/lang.js'></script>
  <script src='assets/js/common.js'></script>
  <script src='//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>

  <script>
    $(document).ready(function() {
      $('.toast').toast('show');
      canyoueatit.initEvents();
    });
  </script>
</html>
