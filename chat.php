<html>
<?php
  if(!isset($_COOKIE['idioma'])){
    setCookie('idioma','ES');
  }
  include_once('session/sessions.php');
  if(isset($_SESSION['user']['id'])){
?>
  <head>

    <?php include_once('includes/meta.html');?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" type='text/css' href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/chat.css'>
    <link href="assets/css/style.min.css" rel="stylesheet">

  </head>
  <body>

    <?php include_once('includes/navbar.php'); ?>
    <div class="content">
      <div class="msgdiv">
        <div class="msgbox">
          <div class="people-list col-lg-4">
            <div class="people-list-search">
              <div class="people-list-header">
                <div class="msgtext"><h4>Recientes</h4></div>
              </div>
              <div class="msg-search">
                <div>
                  <input id="search-person" type="text" class="msg-search-input" placeholder="Buscar"></input>
                  <span class="msg-search-span">
                    <input type="button" class="msg-search-butt"><i class="fa fa-search" aria-hidden="true"></i></input>
                  </span>
                </div>
              </div>
            </div>
            <div class="chat-list">
            </div>
          </div>
          <div class="chat col-lg-8">
            <div class="chat-hist">
            </div>
            <div class="msg-write">
              <div class="msg-write-div">
                <input class="msg-write-msg"></input>
                <input hidden id="user-id" value='<?php echo $_SESSION['user']['id']; ?>'>
                <button class="msg-write-msg-send"><i class="far fa-paper-plane"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/js.js'></script>
  <script src='assets/js/lang.js'></script>
  <script src='assets/js/common.js'></script>
  <script src='assets/js/chat.js'></script>
  <script>
    $(document).ready(function() {
      canyoueatit.initEvents();
    });
  </script>
<?php } else {
  header("Location: index.php");
  exit();
  } ?>
</html>
