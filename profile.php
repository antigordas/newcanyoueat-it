<html>
  <?php
    if(!isset($_COOKIE['idioma'])){
      setCookie('idioma','ES');
    }
    include_once('session/sessions.php');
    if(isset($_SESSION['user']['id'])){
    ?>
  <head>
    <?php include_once('includes/meta.html');?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link href="assets/css/style.min.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href='assets/css/main.css'>
  </head>
  <body>
    <?php include_once('includes/navbar.php'); ?>
    <div class="content">
      <div class="container">
        <div class="row my-2">
          <div class="col-lg-8 order-lg-2">
            <div class="tab-content py-4">
              <form role="form">
                  <input type="text" id="id" class="form-to-send" hidden value="<?php echo $_SESSION['user']['id'];?>">
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-name">{{name}}</span></label>
                    <div class="col-lg-9">
                      <input class="form-control form-to-send" id="nombre" type="text" value="<?php echo $_SESSION['user']['nombre'] ?? null; ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-lastname">{{lastname}}</span></label>
                    <div class="col-lg-9">
                      <input class="form-control form-to-send" id="apellido" type="text" value="<?php echo $_SESSION['user']['apellido'] ?? null;?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-email">{{Email}}<span></label>
                    <div class="col-lg-9">
                      <input class="form-control" type="email" disabled value="<?php echo $_SESSION['user']['email'];?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-user">{{Username}}</span></label>
                    <div class="col-lg-9">
                      <input class="form-control" type="text" disabled value="<?php echo $_SESSION['user']['user'];?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-password">{{Password}}</span></label>
                    <div class="col-lg-9">
                      <input class="form-control form-to-send" id="password" type="password" autocomplete value="11111122333">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"><span class="lang-confirm-password">{{Confirm password}}</label>
                    <div class="col-lg-9">
                      <input class="form-control form-to-send" id="password2" type="password" value="11111122333">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label"></label>
                    <div class="col-lg-9">
                      <button class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-cancel">{{cancel}}</span></button>
                      <button id="profile-save" class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-save">{{save}}</span></button>
                    </div>
                  </div>
                </form>
            </div>
            <div class="col-sm-12">
              <div class="card card-body">
                <h4 class="card-title">¿Tienes algún problema?</h4>
                <form class="form-horizontal mt-4">
                  <div class="form-group">
                    <label>Explícanos que pasa y te enviaremos una respusta por correo.</label>
                    <textarea id="ticket" class="form-control" rows="5"></textarea>
                  </div>
                  <button id="send-ticket" class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-send">enviar</span></button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-4 order-lg-1 text-center">
            <img src="//placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="avatar">
            <h6 class="mt-2"><span class="lang-photo">Upload a different photo<span></h6>
            <label class="custom-file">
            <input type="file" id="file" class="custom-file-input">
            <span class="custom-file-control"><span class="lang-choose">Choose file</span></span>
            </label>
          </div>
        </div>
      </div>

    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/js.js'></script>
  <script src='assets/js/lang.js'></script>
  <script src='assets/js/common.js'></script>
  <script src='assets/js/profile.js'></script>
  <script>
    $(document).ready(function() {
      canyoueatit.initEvents();
      profile.initProfileEvents();
    });
    <?php } else {
      header("Location: index.php");
      exit();
      } ?>
  </script>
</html>
