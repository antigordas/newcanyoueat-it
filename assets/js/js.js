var actualLang = "";
canyoueatit = {

  initEvents: function() {

    /*****************************************************************************************************/
    /* Función; initAdminEvents                                                                          */
    /* Esta funcion inicia todos los eventos del index y da algunas funciones para todos los archivos    */
    /*****************************************************************************************************/

    /*****************************************************************************************************/
    /* Función; loadsLanguage                                                                            */
    /* Esta carga el idioma según la cookie, que es creada en ES por default                             */
    /*****************************************************************************************************/

    loadsLanguage(getCookie('idioma'))
    $('#signin').click(function(e) {

      /*****************************************************************************************************/
      /* evento; Click en #signin                                                                          */
      /* Este evento te creará una modal de login en el caso que esta no exista.                           */
      /*****************************************************************************************************/

      e.preventDefault();
      $('body').append('<div class="wrapper"><form class="login"><p class="title">' + actualLang['login'] + '</p><input type="text" id="user" placeholder="' + actualLang['user'] + '" autofocus/><i class="fa fa-user"></i><input id="password" type="password" placeholder="Password" /><i class="fa fa-key"></i><a href="#">' + actualLang['forgot-password'] + '</a><button><i class="spinner"></i><span class="state">' + actualLang['login'] + '</span></button></form></div>')
      var working = false;
      $(document).mouseup(function(e) {
        var container = $(".login");
        if (!container.is(e.target) && container.has(e.target).length === 0 ) {
          if( $('button.btn.btn-secondary').length == 0){
            $(".wrapper").remove();
          }
        }
      });
      $('.login').on('submit', function(e) {
        e.preventDefault();
        if (working) return;
        working = true;
        var $this = $(this),
          $state = $this.find('button > .state');
        $this.addClass('loading');
        $state.html(actualLang['auth']);

        let datos = {
          action: "conectar_usuario",
          data: {
            user: $('#user').val(),
            pass: $('#password').val()
          }
        }

        canyoueatit.peticionAJAX("controller/user-controller.php", datos)
          .then(data => {
            console.log(data)
            if (data == 1) {
              console.log(data)
              $this.addClass('ok');
              $state.html(actualLang['welcome']);
              setTimeout(function() {
                $state.html(actualLang['signin']);
                $this.removeClass('ok loading');
                working = false;
                document.location.reload()
              }, 2000);

            } else if (data == -1) {
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['error'], actualLang['errorloginone'])
            } else if (data == -2) {
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['error'], actualLang['errorloginone'])
            } else if (data == -3) {
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['error'], actualLang['errorloginthree'])
            } else if (data == -4) {
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['error'], actualLang['errorloginfour'])
            }
            if( data != 1){
              $('button.btn.btn-secondary').click(function(){
                $('#exampleModalCenter').modal('hide')
              })
            }



          })
      });
    });
    $('#signup').click(function(e) {
      /*****************************************************************************************************/
      /* evento; Click en #signup                                                                          */
      /* Este evento te creará una modal de signup en el caso que esta no exista                           */
      /*****************************************************************************************************/
      e.preventDefault();
      $('body').append('<div class="wrapper"><form class="login"><p class="title">' + actualLang['signup'] + '</p><input id="email" type="text" placeholder="' + actualLang['email'] + '" required autofocus/><i class="fa fa-mail"></i><input id="user" type="text" placeholder="' + actualLang['user'] + '" required autofocus/><i class="fa fa-user"></i><input id="password" type="password" placeholder="' + actualLang['password'] + '" required/><i class="fa fa-key"></i><div class="form-group"><div class="form-check"><input class="form-check-input special-align" type="checkbox" value="" id="invalidCheck2" required=""><label class="form-check-label " for="invalidCheck2">' + actualLang['condition-terms'] + '</label></div></div><button><i class="spinner"></i><span class="state">' + actualLang['signup'] + '</span></button></form></div>')

      var working = false;
      $(document).mouseup(function(e) {
        var container = $(".login");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          $(".wrapper").remove();
        }
      });
      $('.login').on('submit', function(e) {
        e.preventDefault();
        if (working) return;
        working = true;
        var $this = $(this),
          $state = $this.find('button > .state');
        $this.addClass('loading');
        $state.html(actualLang['auth']);

        let datos = {
          action: "registrar_usuario",
          data: {
            user: $('#user').val(),
            pass: $('#password').val(),
            email: $('#email').val()
          }
        }
        canyoueatit.peticionAJAX("controller/user-controller.php", datos)
          .then(data => {
            console.log(data)
            if(data == 1){
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['sucessfull'], actualLang['signupsuccess'])
            } else if( data == "-1"){
              $state.html(actualLang['login']);
              $this.removeClass('ok loading');
              canyoueatit.createModal(actualLang['error'], actualLang['signuperrorone'])
            }
          })
          .catch(error => {
            console.log(error)
          })
      })
    })
    $('.lang-drop').click(function(e) {
      /*****************************************************************************************************/
      /* evento; Click en .lang-drop                                                                       */
      /* Este evento te creará cargará el idioma segun el texto del elemento clicado.                      */
      /*****************************************************************************************************/
      loadsLanguage($(this).text())
    })
    $('.logout').click(function(e) {
      /*****************************************************************************************************/
      /* evento; Click en #logout                                                                          */
      /* Este avisará a la base de datos de que te has desconectado                                        */
      /*****************************************************************************************************/
      e.preventDefault()
      let datos = {
        action: "logout"
      }
      canyoueatit.peticionAJAX("controller/user-controller.php", datos)
        .then(data => {
          document.location.reload()
        })
    })
    $('.food-p').click(function() {
      /*****************************************************************************************************/
      /* evento; Click en .food-p                                                                          */
      /* Este evento te creará una modal (si no está creada) donde están todas las alergias                */
      /*****************************************************************************************************/
      canyoueatit.listaTranstornos()
    })
    $('body').on('hidden.bs.modal', function (e) {
      /*****************************************************************************************************/
      /* evento; hidden en una modal de bootstrap                                                          */
      /* Este borrará la modal en el caso que esta se esconda                                              */
      /*****************************************************************************************************/
      working = false;
      $('#notificacion').remove()
    })
    $('body').on('click','.delete-food-modal', function(){
      /*****************************************************************************************************/
      /* evento; Click en .delete-food-modal                                                               */
      /* Este evento te esconderá la modal de los alergenos, para no volver a hacer otra peticion          */
      /*****************************************************************************************************/
      $('#foodModal').modal('hide')
    })
    $('body').on('click','.search-button', function(){
      /*****************************************************************************************************/
      /* evento; Click en .search-button                                                                   */
      /* Este evento hará una petición a la DB para sacar una lista de la comida que puedes comer dados    */
      /* unos alergenos concretos                                                                          */
      /*****************************************************************************************************/

      if($('.relleno-prueba').length > 0 ){
        $('.relleno-prueba').remove()
      }
      $('.food-p').val().split(', ')
      array = {}
      $('.custom-control-input').each(function(){
        if($(this).prop('checked') == true){
          array[$(this).attr('id')] = true
        } else {
          array[$(this).attr('id')] = false
        }
      })

      let datos = {
        action: "buscar_comida",
        data: {
          disseases: array,
          food: $('#food-to-search').val()
        }
      }
      canyoueatit.peticionAJAX('controller/food-controller.php', datos)
        .then( data => {
          console.log(data)
          toAppend = ""
          data = JSON.parse(data)
          Object.keys(data).forEach(function(item){
            thisItem = data[item]
            toAppend += '<tr><td>'+thisItem['nombre']+'</td><td>'+thisItem['composicion']+'</td><td>'+thisItem['alergenos']+'</td></tr>'
          })
          if ($.fn.DataTable.isDataTable( '#tabla-index' ) ) {
            $('#tabla-index').DataTable().destroy()
            $('tbody').empty()
          }
          $('tbody').append(toAppend)
          $('#tabla-index').DataTable(TABLE_ES)
        })
        .catch( error => {
          console.error(error)
        })
    })
    $('body').on('click','.toast_sugerencia', function(e){
      e.preventDefault()
      $('body').append('<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">' + actualLang['sugerencia'] + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div class="form-group"> <label>'+actualLang['sugerencia-aqui']+'</label> <textarea id="sugerencia" class="form-control" rows="5"></textarea> </div></div><div class="modal-footer"><button type="button" class="btn btn-primary send-sugges" data-dismiss="modal">enviar</button></div></div>')
      $('.modal').modal('show')
    })
    $('body').on('click', '.send-sugges', function(){
      let datos = {
        action: "enviar_sugerencia",
        data: {
          mensaje: $('#sugerencia').val()
        }
      }

      canyoueatit.peticionAJAX('controller/user-controller.php', datos)
        .then(data => {
          console.log(data)
        })
        .catch(error => {
          console.error(error)
        })
    })
  },
  peticionAJAX: function(url, datos) {
    /*****************************************************************************************************/
    /* función: peticionAJAX()                                                                           */
    /* Esta función lanza una petición AJAX al servidor dada una URL y unos datos, la función devuelve   */
    /* una promesa para tratar los datos una vez acabe                                                   */
    /*****************************************************************************************************/
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: url,
        data: {
          action: datos.action,
          datos: datos.data
        },
        success: function(jsonData, callback) {
          resolve(jsonData)
        },
        error: function(error) {
          reject(error)
        }
      })
    })
  },
  createModal: function(title, message) {
    /*****************************************************************************************************/
    /* función: createModal()                                                                            */
    /* Esta función creará una modal multiusos para enseñar errores y otros                              */
    /*****************************************************************************************************/
    $('body').append('<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">' + title + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">' + message + '</div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">'+actualLang['close']+'</button></div></div>')
    $('.modal').modal('show')
  },
  confirmarCorreo: function(email, hash) {
    /*****************************************************************************************************/
    /* función: confirmarCorreo()                                                                        */
    /* Esta función mirará si los parametros pasados por get son correctos y si existen ambos, entonces  */
    /* lo confirmará en la base de datos                                                                 */
    /*****************************************************************************************************/
    if(email != null && hash != null){

      let datos = {
        action: "confirmar_correo",
        data : {
          email: email,
          hash: hash
        }
      }
      canyoueatit.peticionAJAX("controller/user-controller.php", datos)
        .then( data => {
          console.log(data)
          if(data == "1"){
            console.log(actualLang)
            canyoueatit.createModal("Email verificado con éxito", 'El email ha sido verificado correctamente')
          } else {
            console.log(actualLang)
            canyoueatit.createModal("Error", 'El correo ya ha sido verificado anteriormente')
          }
          $('button').click(function(){
            window.location.replace("index.php");
          })
        })
    } else {
      createModal("Has intentado entrar como no debías.", "Error")
    }
  },
  listaTranstornos: function (){
    /*****************************************************************************************************/
    /* función: listaTranstornos()                                                                       */
    /* esta función cargará la lista de transtornos alimenticios de la db y creará una modal una vez     */
    /*****************************************************************************************************/
    if($('#foodModal').length == 0){
      let datos = {
        action: 'cargar_problemas'
      };
      canyoueatit.peticionAJAX('controller/food-controller.php',datos)
        .then(data => {
          console.log(data)
          let modal = '<div class="modal fade" id="foodModal" tabindex="-1" role="dialog" aria-labelledby="foodModal" aria-hidden="true"> <div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"> <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Lista de transtornos</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button> </div> <div class="modal-body">'
          let modalFooter = '</div><div class="modal-footer"> <button type="button" class="btn btn-secondary delete-food-modal" data-dismiss="modal">'+actualLang['close']+'</button><button type="button" class="btn btn-primary guardar" data-dismiss="modal">'+actualLang['save']+'</button></div></div>'

          data = $.parseJSON(data);
          data.forEach(function(item) {
            let id = item.id, transtorno = item.transtorno, alergenos  = item.alergenos
            modal += '<div class="custom-control custom-checkbox"> <input type="checkbox" value="'+alergenos+'" class="custom-control-input" id="'+alergenos+'"> <label class="custom-control-label" for="'+alergenos+'">'+transtorno+'</label></div>'
          })
          $('body').append(modal+modalFooter)
          $('.modal').modal('show')
          $('.guardar').click(function() {
            var valores = "";
            $('.custom-control-input:checked').each(function() {
              valores += $(this).attr('value') + ", "
            })
            $('.food-p').val(valores.slice(0, -2))
          })
        })
        .catch( error => {
          console.error(error)
          console.log("ha ocurrido un error")
        })
    } else {
      $('#foodModal').modal('show')
    }
  }
}
