admin = {
  initAdminEvents: function () {

    /*****************************************************************************************************/
    /* Función; initAdminEvents                                                                          */
    /* Esta funcion se carga al cargar la página e inicia todos los eventos del panel de administrador   */
    /*****************************************************************************************************/

    $('.users-table').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .users-table                                                            */
      /* Este evento lanzará una petición al servidor para coger algunos de los datos de los Usuarios.     */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getUsers()
    })
    $('.super-table').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .supers-table                                                           */
      /* Este evento lanzará una petición al servidor para coger algunos de los datos de los Supermercados */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getSupers()
    })
    $('.products-table').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .products-table                                                         */
      /* Este evento lanzará una petición al servidor para coger los datos de los productos                */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getProducts()
    })
    $('.tickets-table').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .tickets-table                                                          */
      /* Este evento lanzará una petición al servidor para coger los datos de tickets de los usuarios      */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getTickets()
    })
    $('.meats-problems-table').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .meats-problems-table                                                   */
      /* Este evento lanzará una petición al servidor para coger los datos de los problemas alimentarios   */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getDissorders()
    })
    $('.suggestion-list').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .suggestion-list                                                        */
      /* Este evento lanzará una petición al servidor para coger las sugerencias                           */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      admin.getSugerencias()
    })
    $('.to-do-panel').click(function (e) {

      /*****************************************************************************************************/
      /* evento; Click en la clase .suggestion-list                                                        */
      /* Iba a implementar el plugin Lobilist que sirve para hacer algo parecido a un trello.              */
      /*****************************************************************************************************/

      e.preventDefault()
      admin.destroyTable()
      // $('#locura').lobiList({
      //   defaultStyle: 'lobilist-info',
      //   lists: [{
      //     defaultStyle: 'lobilist-danger',
      //   },]
      // });
      // admin.getTodo()
    })
    $('body').on('click', '#addRow', function () {

      /*****************************************************************************************************/
      /* evento; Click en el id #AddRowModal                                                               */
      /* Este evento te abrirá una modal que te pedirá los datos marcados con la clase th-required para    */
      /* añadir la Row en la base de datos y en el visual                                                  */
      /*****************************************************************************************************/

      let modal = '<div class="modal fade" id="AddRowModal" tabindex="-1" role="dialog" aria-labelledby="AddRowModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title" id="AddrowModal">Modal title</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>  <div class="modal-body"><form>'
      $('th.c-required').each(function (i) {
        console.log($(this).text())
        modal += '<div class="form-group"><label for="' + $(this).text() + '" class="control-label">' + actualLang[$(this).text()] + '</label><input type="text" class="form-control add-required" id="' + $(this).text() + '"></div>'
      })

      if ($('.table-active').text() == "Usuarios") {
        modal += '<div class="form-group"><label for="' + actualLang['password'] + '" class="control-label">' + actualLang['password'] + '</label><input type="password" class="form-control" id="password"></div>'
      }

      modal += '</div> <div class="modal-footer"> <button type="button" class=" btn btn-secondary" data-dismiss="modal">' + actualLang['close'] + '</button> <button id="save-row" type="button" class=" btn btn-primary">' + actualLang['save'] + '</button> </div> </div> </div> </div> '
      $('body').append(modal)
      $("#AddRowModal").modal('show');
    })
    $('body').on('hidden.bs.modal', function (e) {

      /*****************************************************************************************************/
      /* evento; bootstrapHideModal es un evento de Bootstrap conforme una modal se ha escondido con Hide  */
      /* Este evento borrará una modal del DOM si esta se esconde, para no reutilizarla                    */
      /*****************************************************************************************************/

      $('#AddRowModal').remove();
    });
    $('body').on('click', '.table-link', function () {

      /*****************************************************************************************************/
      /* evento; Click en la clase .table-link                                                             */
      /* Esta clase es para saber que base de datos se está visualizando y pasarla al Backend si s necesita*/
      /*****************************************************************************************************/

      $('.table-active').removeClass('table-active')
      $(this).addClass('table-active')
    })
    $('body').on('click', '#save-row', function () {

      /*****************************************************************************************************/
      /* evento; Click en la id #save-rows                                                                 */
      /* Este evento lanzará una petición al servidor para añadir un elemento en la base de datos          */
      /* desde el panel de administrador y la insertará en la vista actual de la tablas                    */
      /*****************************************************************************************************/

      let array = new Array($('th').length).fill(null)

      $('th').each(function (i) {
        text = $(this).text()
        $('.add-required').each(function (b) {
          if (text == $(this).attr('id')) {
            array[i] = $(this).val()
          }
        })
      })

      data = {}
      $('.add-required').each(function (i) {
        data[$(this).attr('id')] = $(this).val()
      })

      let datos = {
        data: {
          table: $('.table-active').attr('value'),
          tablas: data
        },
        action: 'add_by_table'
      };

      admin.peticionAJAXAdmin('controller/admin-controller.php', datos)
        .then(data => {
          array[0] = data;
          $('#dataTable').DataTable().row.add(array).draw()
        })
        .catch(error => {
          console.error(error)
        })
    })
    $('body').on('click', '.fa-edit', function () {

      /*****************************************************************************************************/
      /* evento; Click en la clase .fa-edit                                                                */
      /* Este evento creará una modal con los datos del tr actual                                          */
      /*****************************************************************************************************/

      let thisRow = $(this).parent().parent().find('td')
      let modal = '<div class="modal fade" id="AddRowModal" tabindex="-1" role="dialog" aria-labelledby="AddRowModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title" id="AddrowModal">' + actualLang['update'] + '</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>  <div class="modal-body"><form>'
      $(thisRow).each(function (i) {
        if (!$(this).hasClass('no-editable')) {
          thRow = $(this).closest('table').find('th').eq($(this).index()).text()
          if (thRow == "id") {
            modal += '<div class="form-group"><label for="' + thRow + '" class="control-label">' + thRow + '</label><input type="text" readonly class="form-control add-required" value="' + $(this).text() + '" id="' + thRow + '"></div>'
          } else {
            modal += '<div class="form-group"><label for="' + thRow + '" class="control-label">' + thRow + '</label><input type="text" class="form-control add-required" value="' + $(this).text() + '" id="' + thRow + '"></div>'
          }
        }
      })

      modal += '</div> <div class="modal-footer"> <button type="button" class=" btn btn-secondary" data-dismiss="modal">' + actualLang['close'] + '</button> <button id="update-row" type="button" class=" btn btn-primary">' + actualLang['update'] + '</button> </div> </div> </div> </div> '
      $('body').append(modal)
      $('#AddRowModal').modal('show')
    })
    $('body').on('click', '#update-row', function () {

      /*****************************************************************************************************/
      /* evento; Click en la id #update-row                                                                */
      /* Este evento lanzará una petición de update a la base de datos                                     */
      /*****************************************************************************************************/

      let toUpdate = {}
      $('.add-required').each(function () {
        if ($(this).text() != null) {
          toUpdate[$(this).attr('id')] = $(this).val()
        }
      })
      let datos = {
        data: {
          rol: 1,
          tabla: $(".table-active").attr('value'),
          a_actualizar: toUpdate
        },
        action: 'actualizar_tabla'
      }
      admin.peticionAJAXAdmin('controller/admin-controller.php', datos)
        .then(data => {
          console.log(data)
        })
        .catch(error => {
          console.log(error)
        })
    })
  },
  getUsers: function () {

    /*****************************************************************************************************/
    /* función: getUsers()                                                                               */
    /* Esta función lanza la petición de coger los datos de los usuarios de la db                        */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_usuarios'
    };
    admin.peticionAJAXAdmin('controller/user-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-striped table-bordered"><thead><th>id</th><th class="c-required">user</th><th class="c-required">email</th><th>confirmed</th><th>active</th><th>reg_date</th><th>last_seen</th><th>rol</th><th>visible</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody></table>')
        jsonValues.forEach(function (item) {
          $('.tbody-db').append('<tr data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.user + '</td><td>' + item.email + '</td><td>' + item.confirmed + '</td><td>' + item.active + '</td><td class="no-editable">' + item.reg_date + '</td><td class="no-editable">' + item.last_seen + '</td><td>' + item.rol + '</td><td>' + item.visible + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></td></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  getSupers: function () {

    /*****************************************************************************************************/
    /* función: getSupers()                                                                              */
    /* Esta función lanza la petición de coger los datos de los supermercados de la db                   */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_supers'
    };
    admin.peticionAJAXAdmin('controller/food-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-striped table-bordered"><thead><th>id</th><th class="c-required">nombre</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody>')
        jsonValues.forEach(function (item) {
          $('.tbody-db').append('<tr data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.nombre + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></td></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  getProducts: function () {

    /*****************************************************************************************************/
    /* función: getProducts()                                                                            */
    /* Esta función lanza la petición de coger los datos de los productos de la db                       */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_productos'
    };
    admin.peticionAJAXAdmin('controller/food-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-striped table-bordered"><thead><th>id</th><th class="c-required">nombre</th><th>link_compra_supermercado</th><th class="c-required">Composicion</th><th class="c-required">alergenos</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody>')
        jsonValues.forEach(function (item) {
          $('.tbody-db').append('<tr data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.nombre + '</td><td>' + item.link_compra_supermercado + '</td><td>' + item.composicion + '</td><td>' + item.alergenos + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  getTickets: function () {

    /*****************************************************************************************************/
    /* función: getTickets()                                                                             */
    /* Esta función lanza la petición de coger los datos de los tickets en la db                         */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_tickets'
    };
    admin.peticionAJAXAdmin('controller/user-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-bordered"><thead><th>id</th><th class="c-required">problema</th><th>enviado_por</th><th class="c-required">estado</th><th class="c-required">respuesta</th><th class="c-required">fecha</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody>')
        jsonValues.forEach(function (item) {
          let color = ""
          if (item.estado == 0) color = "white"
          if (item.estado == 1) color = "#ffd40073"
          if (item.estado == 2) color = "#00ff6673"
          if (item.estado == -1) color = "#ff000073;"
          $('.tbody-db').append('<tr style="background-color:' + color + '" data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.problema + '</td><td>' + item.enviado_por + '</td><td>' + item.estado + '</td><td>' + item.respuesta + '</td><td>' + item.fecha + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  getSugerencias: function () {

    /*****************************************************************************************************/
    /* función: getSugerencias()                                                                         */
    /* Esta función lanza la petición de coger los datos de las sugerencias en la db                     */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_sugerencias'
    };
    admin.peticionAJAXAdmin('controller/user-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-bordered"><thead><th>id</th><th class="c-required">sugerencia</th><th class="c-required">estado</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody>')
        jsonValues.forEach(function (item) {
          let color = ""
          if (item.estado == 0) color = "#ffd40073"
          if (item.estado == 1) color = "#00ff6673"
          if (item.estado == -1) color = "#ff000073;"
          $('.tbody-db').append('<tr style="background-color:' + color + '" data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.sugerencia + '</td><td>' + item.estado + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  getDissorders: function () {

    /*****************************************************************************************************/
    /* función: getDissorders()                                                                          */
    /* Esta función lanza la petición de coger los datos de los problemas alimenticios                           */
    /*****************************************************************************************************/

    let datos = {
      data: {
        rol: 1
      },
      action: 'cargar_problemas'
    };
    admin.peticionAJAXAdmin('controller/food-controller.php', datos)
      .then(data => {
        jsonValues = $.parseJSON(data);
        $('#table-support').append('<table id="dataTable" class="users-table table table-striped table-bordered"><thead><th>id</th><th class="c-required">Transtorno</th><th class="c-required">alergenos</th><th>Opciones adicionales</th></thead><tbody class="tbody-db"></tbody>')
        jsonValues.forEach(function (item) {
          $('.tbody-db').append('<tr data-user-list="' + item.id + '"><td>' + item.id + '</td><td>' + item.transtorno + '</td><td>' + item.alergenos + '</td><td class="no-editable"><i class="fa fa-edit admin-icon" value="' + item.id + '" aria-hidden="true"></i></tr>');
        })
        $('#dataTable').DataTable(TABLE_ES)
        admin.createAddButton()
      })
      .catch(error => {
        console.error(error)
      })
  },
  // getTodo: function() {
  //   let datos = {
  //     data: {
  //       rol: 1
  //     },
  //     action: 'cargar_problemas'
  //   };
  //   admin.peticionAJAXAdmin('controller/food-controller.php', datos)
  //     .then(data => {
  //       jsonValues = $.parseJSON(data);
  //       jsonValues.forEach(function(item) {
  //         console.log(item)
  //       })
  //     })
  //     .catch(error => {
  //       console.error(error)
  //     })
  // },
  destroyTable: function () {

    /*****************************************************************************************************/
    /* función: destroyTable()                                                                           */
    /* Esta función limpia la tabla actual                                                               */
    /*****************************************************************************************************/

    $('#dataTable').DataTable().destroy()
    $('#table-support').empty()
  },
  peticionAJAXAdmin: function (url, datos) {
    /*****************************************************************************************************/
    /* función: peticionAJAXAdmin()                                                                      */
    /* Esta función lanza una petición AJAX al servidor dada una URL y unos datos, la función devuelve   */
    /* una promesa para tratar los datos una vez acabe                                                   */
    /*****************************************************************************************************/
    return new Promise(function (resolve, reject) {
      $.ajax({
        type: 'POST',
        url: url,
        data: {
          action: datos.action,
          datos: datos.data
        },
        success: function (jsonData, callback) {
          if (jsonData == '-1') {
            alert("No tienes permisos para hacer esa acción");
          } else {
            resolve(jsonData)
          }
        },
        error: function (error) {
          reject(error)
        }
      })
    })
  },
  createAddButton: function () {

    /*****************************************************************************************************/
    /* función: createAddButton()                                                                        */
    /* Esta función crea un botón para añadir algo en la base de datos                                   */
    /*****************************************************************************************************/

    $('#table-support').append('<button id="addRow" class="btn btn-info" data-toggle="modal" data-target="#AddRowModal">' + actualLang['add-new-row'] + '</button>')
  }
}
