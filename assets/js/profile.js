profile = {
  initProfileEvents: function() {
    $('body').on('click','#profile-save', function(e){

      /*****************************************************************************************************/
      /* evento: click en #profile-save                                                                    */
      /* actualizará si es necesario el perfil, pero no la foto, todavía                                   */
      /*****************************************************************************************************/
      e.preventDefault()

      if($('#password').val() == $('#password2').val() ){
        let temporalArray = {};
        $('.form-to-send').each(function(e){
          if($(this).val().length > 0 && $(this).val() != "11111122333") temporalArray[$(this).attr('id')] = $(this).val()
        })
        let datos = {
          action: "update_profile",
          data: temporalArray
        }
        console.log(datos)

        profile.peticionAJAX('controller/user-controller.php', datos)
          .then(data => {
            console.log(data)
          })
          .catch(error => {
            console.error(error)
          })
      } else {
        canyoueatit.createModal(actualLang['password-rel-error'], actualLang['password-rel-error2'])
      }

    })
    $('body').on('click','#send-ticket', function(e){
      /*****************************************************************************************************/
      /* evento: click en #send-ticket                                                                     */
      /* este evento enviará un ticket cogiendo la id y el texto                                           */
      /*****************************************************************************************************/
      e.preventDefault()
      let datos = {
        action: "enviar_ticket",
        data: {
          id: $('#id').val(),
          mensaje: $('#ticket').val()
        }
      }

      canyoueatit.peticionAJAX('controller/user-controller.php', datos)
        .then(data => {
          console.log(data)
        })
        .catch(error => {
          console.error(error)
        })
    })
  },
  peticionAJAX: function(url, datos) {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: url,
        data: {
          action: datos.action,
          datos: datos.data
        },
        success: function(jsonData, callback) {
          if (jsonData == '-1') {
            alert("No tienes permisos para hacer esa acción");
          }
          else {
            resolve(jsonData)
          }
        },
        error: function(error) {
          reject(error)
        }
      })
    })
  },
}
