
$(window, document, undefined).ready(function() {
  var reloadAjaxChat;
  cargarRecientes( $('#user-id').val() )

  /***********************************************************************************************************************************/
  /* Evento: keyup                                                                                                                   */
  /* En: #search-person                                                                                                              */
  /* Evento Keyup en el buscador de personas. Solo realizará la búsqueda en el caso que el nombre del usuario escrito tenga más de 3 */
  /* letras. En el caso en el que tenga más de 3 letras, llamará  una función AJAX que realizará la petición correspondiente a la bd */
  /* FALTA: Nada                                                                                                                     */
  /***********************************************************************************************************************************/

  $( '#search-person' ).keyup(function () {
    if ($(this).val().length > 3) {
      var name = $(this).val()
      if($("h5").length == 0){
        buscarMasChats(name)
      }
      $("h5").each( function (index) {

        if ($(this).text().indexOf(name) < 0) {
          console.log(name)
          buscarMasChats(name)
        }
      })
    }

  });

  /***********************************************************************************************************************************/
  /* Evento: click                                                                                                                   */
  /* En: .msg-write-msg-send                                                                                                         */
  /* Evento Click al botón de enviar un mensaje en el chat. El mensaje solo se enviará en el caso de que el mensaje no esté vacío.   */
  /* FALTA: Controlar los {}                                                                                                         */
  /***********************************************************************************************************************************/

  $( '.msg-write-msg-send' ).click(function () {

    if ($(".msg-write-msg").val().length > 0) {

      enviarMensaje($("#user-id").val(), $('.msg-chat-box-active').attr('data-value'), $(".msg-write-msg").val())
      setTimeout(function(){
        document.querySelector('.msg-write').scrollIntoView({
          behavior: 'smooth'
        });
      }, 200)

      $(".msg-write-msg").val("")

    };

  })

  /***********************************************************************************************************************************/
  /* Evento: keypress                                                                                                                */
  /* En: .msg-write-msg                                                                                                              */
  /* Evento Enter que envía un mensaje al chat. El mensaje solo se enviará en el caso de que el mensaje no esté vacío.               */
  /* FALTA: Nada                                                                                                                     */
  /***********************************************************************************************************************************/

  $( '.msg-write-msg' ).keypress(function (e){

    if(e.which == 13) $('.msg-write-msg-send').trigger('click')

  })

  /***********************************************************************************************************************************/
  /* Evento: click                                                                                                                  */
  /* En: .msg-chat-box, creado dinámicamente dentro de .chat-list                                                                    */
  /* Evento Click a un Panel de usuario dentro de la lista de chats. Hace falta especificar dentro de donde se ejecutará el evento   */
  /* dado que son unos elementos creados de manera dinámica con JS                                                                   */
  /* FALTA: Nada                                                                                                                     */
  /***********************************************************************************************************************************/

  $( '.chat-list' ).on('click', '.msg-chat-box', async function (){

    if(!$(this).hasClass('msg-chat-box-active')){

      $('.msg-chat-box-active').removeClass('msg-chat-box-active')
      $(this).addClass('msg-chat-box-active')
      bajarChat()

      let userId = $('#user-id').val()
      let otherId = $(this).attr('data-value')

      $('.chat-hist').empty()
      loadSpecificChat(userId, otherId)

    }
  })

  function buscarMasChats(name) {
    $.ajax({
      type:'POST',
      url: "controller/user-controller.php",
      data: { action: 'buscar_mas_chats', name: name },
      success: function(data, callback) {
        data = $.parseJSON(data)
        data.forEach(function(item) {
          id    = item.id
          name  = item.user
          exist = false;

          $("h5").each(function() {
            if(name == $(this).text()) exist = true;
          })

          if(!exist) $(".chat-list").append('<div class="msg-chat-box" data-value='+id+'><div class="msg-chat-context"><div class="msg-chat-img"><img class="msg-chat-little-img" src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="msg-chat-text"><h5>'+name+'</h5><input hidden="" class="user-id"></div></div></div>')

        })
      },
      error: function(error) {
        console.log(error)
      }
    })
  }

  function loadSpecificChat(userid, contactid) {
  $.ajax({
    type:'POST',
    url: "controller/user-controller.php",
    data: { action: 'specific_chat', userid: userid, contactid: contactid },
    success: function(jsonData, callback) {

      jsonValues = $.parseJSON(jsonData)
      jsonValues.forEach(function(item) {

        if( userid == item.sent_by_user_id ) {
          $('.chat-hist').append('<div class="msg-sent"><div class="msg-sent-text"><p class="msg-sent-text-p">'+item.message+'</p><span class="msg-timedate">'+item.time+'</span></div></div>')
        } else {
          $('.chat-hist').append('<div class="msg-received"><div class="msg-received-text"><p class="msg-received-text-p">'+item.message+'</p><span class="msg-timedate">'+item.time+'</span></div></div>')
        }

      });

      var userId        = $('#user-id').val(),
          otherId       = $('.msg-chat-box-active').attr('data-value'),
          totalMensajes = $('.msg-sent').length + $('.msg-received').length;

      cargarNuevosMensajes(userId, otherId, totalMensajes)
    },
    error: function(error) {
      console.log(error)
    }
  })
}

  function enviarMensaje(userid, contactid, message) {
  $.ajax({
    type:'POST',
    url: "controller/user-controller.php",
    data: { action: 'send_message', userid: userid, contactid: contactid, message: message },
    success: function(data, callback) {
      console.log(data)
      data = $.parseJSON(data)
      if(data) {
        $('.chat-hist').append('<div class="msg-sent"><div class="msg-sent-text"><p class="msg-sent-text-p">'+data[0].message+'</p><span class="msg-timedate">'+data[0].time+'</span></div></div>')
        bajarChat()
      }
    },
    error: function(error) {
      console.log(error)
    }
  })
  }

  /***********************************************************************************************************************************/
  /* Nombre función: cargarNuevosMensajes(userId, otherId, totalMensajes)                                                            */
  /* Relacionado con: Messages                                                                                                       */
  /* Petición AJAX que espera una respuesta del servidor durante unos segundos, si el servidor no responde vuelve a llamarse con los */
  /* mismos datos. Si se carga otro usuario, esta petición se cancelará.                                                             */
  /* FALTA: Nada                                                                                                                     */
  /***********************************************************************************************************************************/

  function cargarNuevosMensajes(userId, otherId, totalMensajes) {
    var otherId2   = otherId,
        userId2    = userId;

    reloadAjaxChat = $.ajax({
      type:'POST',
      url: "controller/user-controller.php",
      data: { action: 'sleep_chat', userid: userId, contactid: otherId, totalMensajes: totalMensajes },
      beforeSend: function() {
        if(reloadAjaxChat != null) {
          reloadAjaxChat.abort();
        }
      },
      success: function(item, callback) {

        if(item != "") {
          console.log(item);
          item = $.parseJSON(item);
          if( otherId == item.sent_by_user_id && otherId2 == $('.msg-chat-box-active').attr('data-value')) {
            $('.chat-hist').append('<div class="msg-sent"><div class="msg-sent-text"><p class="msg-sent-text-p">'+item.message+'</p><span class="msg-timedate">'+item.time+'</span></div></div>');
          } else if( userId2 == item.received_by_user_id && otherId2 == $('.msg-chat-box-active').attr('data-value')) {
            $('.chat-hist').append('<div class="msg-received"><div class="msg-received-text"><p class="msg-received-text-p">'+item.message+'</p><span class="msg-timedate">'+item.time+'</span></div></div>');
          }

          bajarChat();
        }

        var userId        = $('#user-id').val(),
            otherId       = $('.msg-chat-box-active').attr('data-value'),
            totalMensajes = $('.msg-sent').length + $('.msg-received').length;
        cargarNuevosMensajes(userId, otherId, totalMensajes);

      },
      error: function(error) {
        console.log(error);
      }
    })
  }

  /***********************************************************************************************************************************/
  /* Nombre función: cargarRecientes(userid)                                                                                         */
  /* Relacionado con: Messages                                                                                                       */
  /* Petición AJAX para cargar el último mensaje que hayas enviado o recibido de otros usuarios.                                     */
  /* FALTA: Nada.                                                                                                                    */
  /***********************************************************************************************************************************/

  function cargarRecientes(userid) {
    $.ajax({
      type:'POST',
      url: "controller/user-controller.php",
      data: { action: 'cargar_recientes', userid: userid},
      success: function(jsonData, callback) {
        jsonValues = $.parseJSON(jsonData)
        jsonValues.forEach(function(item) {
          $('.chat-list').append('<div class="msg-chat-box msg-chat-box" data-value="'+item.received_by_user_id+'"><div class="msg-chat-context"><div class="msg-chat-img"><img class="msg-chat-little-img" src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="msg-chat-text"><h5>'+item.user+'</h5><span>'+item.time+'</span><p>'+item.message+'</p></div></div></div>');
        });
      },
      error: function(error) {
        console.log(error);
      }
    })
  }

})

/***********************************************************************************************************************************/
/* Nombre función:  bajarChat()                                                                                                    */
/* Relacionado con: Messages                                                                                                       */
/* Función para bajar el chat con una animación hasta la parte de abajo del chat.                                                  */
/* FALTA: Nada.                                                                                                                    */
/***********************************************************************************************************************************/

function bajarChat(){
  setTimeout(function(){
    document.querySelector('.msg-write').scrollIntoView({
      behavior: 'smooth'
    });
  }, 200)
}
