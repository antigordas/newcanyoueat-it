<nav class="navbar navbar-expand-lg navbar-light nav-color">
  <a class="navbar-brand" href="#">Can you eat-it</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="main-nav">

    <?php if($_SESSION['user']['log'] > 0) { ?>
    <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
      <?php if ($_SESSION['user']['rol'] > 0){ ?>
        <li class="nav-item">
          <a class="nav-link text-white" href="administracion.php"><span class="lang-admin">{{admin}}</span></a>
        </li>
      <?php } ?>
      <li class="nav-item active">
        <a class="nav-link text-white" href="index.php"><span class="lang-home">{{home}}</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="chat.php"><span class="lang-chat">{{chat}}</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="lang-language">{{idioma}}</span></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item lang-drop" href="#">ES</a>
          <a class="dropdown-item lang-drop" href="#">CA</a>
          <a class="dropdown-item lang-drop" href="#">EN</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="lang-options">{{options}}</span></a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="profile.php"><span class="lang-profile">{{profile}}</span></a>
          <a class="dropdown-item  logout" href="#"><span class="lang-logout">{{logout}}</span></a>
        </div>
      </li>
    </ul>
    <?php } else if( $_SESSION['user']['log'] == 0){?>
    <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
      <form class="form-inline my-2 my-lg-0">
        <button id="signin" class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-login">{{login}}</span></button>
        <button id="signup" class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-signup">{{signup}}</span></button>
      </form>
    </ul>
      <?php } ?>
  </div>
</nav>
