<html>
<?php
  if(!isset($_COOKIE['idioma'])){
    setCookie('idioma','ES');
  }
  include_once('session/sessions.php');
?>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" /><meta charset='utf-8'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link rel='canonical' href='https://www.canyoueat-it.com'>
    <title>Can you eat-it?</title>
    <meta name='author' content='Jose Antonio Aldaz'>
    <meta name='description' content='Canyoueat-it es un buscador hecho por estudiantes de grado superior. Canyouea-it es un buscador de productos para gente con problemas alimenticios.'>
    <meta name='theme-color' content='#2da030'>
    <meta name='robots' content='index, follow, noodp'/>
    <meta name='GoogleBot' content='index, follow, noodp'/>
    <meta property='og:description' content='Canyoueat-it es un buscador hecho por estudiantes de grado superior. Canyouea-it es un buscador de productos para gente con problemas alimenticios.'>
    <meta property='og:title' content='Can you eat-it?'>
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link href="assets/css/style.min.css" rel="stylesheet">
  </head>
  <body>
    <?php include_once('includes/navbar.php'); ?>
    <input id="email" type="text" hidden value="<?php if(isset($_GET['email'])){echo $_GET['email'];}else{echo null;} ?>">
    <input id="hash" type="text" hidden value="<?php if(isset($_GET['hash'])){echo $_GET['hash'];}else{echo null;} ?>">
    <div class="content">
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/lang.js'></script>
  <script src='assets/js/js.js'></script>
  <script src='assets/js/common.js'></script>
  <script>
    $(document).ready(function(){
      $(window).on('load', function() {
        canyoueatit.confirmarCorreo($('#email').val(), $('#hash').val());
      })
    })
  </script>
</html>
