<html>
<?php
  if(!isset($_COOKIE['idioma'])){
    setCookie('idioma','ES');
  }
  include_once('session/sessions.php');

  if(isset($_SESSION['user']['rol']) && $_SESSION['user']['rol'] > 0){

?>
  <head>

    <?php include_once('includes/meta.html');?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link rel="stylesheet" type='text/css' href="assets/css/style.min.css" >
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/admin.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/dataTables.css'>
    <!-- <link rel='stylesheet' type='text/css' href='assets/css/jquery-ui.min.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/lobilist.min.css'> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
  </head>
  <body>
    <?php include_once('includes/navbar.php'); ?>
    <nav class="navbar navbar-expand-lg navbar-light secondary-nav" >
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#secondary-nav-nav" aria-controls="secondary-nav-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="secondary-nav-nav">
        <ul class="navbar-nav mt-2 mt-lg-0 mx-auto order-0">
          <li class="sidebar-item">
            <a class="nav-link users-table table-active table-link" value="user"><span class="lang-users">Usuarios</span></a>
          </li>
          <li class="sidebar-item">
            <a class="nav-link super-table table-link" value="supermarkets"><span class="lang-supers">Supermercados</span></a>
          </li>
          <li class="sidebar-item">
            <a class="nav-link products-table table-link" value="product"><span class="lang-products">Productos</span></a>
          </li>
          <li class="sidebar-item">
            <a class="nav-link meats-problems-table table-link" value="alimentary_disorder"><span class="lang-meat-problems">Problemas alimenticios</span></a>
          </li>
          <li class="sidebar-item">
            <a class="nav-link tickets-table table-link" value="tickets"><span class="lang-tickets">Tickets</span></a>
          </li>
          <li class="sidebar-item">
            <a class="nav-link suggestion-list table-link" value="suggestions"><span class="lang-sugges">Sugerencias</span></a>
          </li>
          <!-- <li class="sidebar-item">
            <a class="nav-link to-do-panel table-link" value="todolist"><span class="lang-todo">To do List</span></a>
          </li> -->
        </ul>
      </div>
    </nav>
    <div class="content col-md-12" id="table-support">
    </div>
    <div class="content col-md-12" id="locura">
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <!-- <script src='assets/js/jquery-ui.min.js'></script>
  <script src='assets/js/jquery.ui.touch-punch-improved.js'></script>
  <script src='assets/js/lobilist.min.js'></script> -->
  <script src='assets/js/js.js'></script>
  <script src='assets/js/admin.js'></script>
  <script src='assets/js/lang.js'></script>
  <script src='assets/js/common.js'></script>
  <script src='//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>

  <script>
    $(document).ready(function() {
      canyoueatit.initEvents();
      admin.initAdminEvents();
      admin.getUsers()
    });
  </script>
<?php } else {
  header("Location: index.php");
  exit();
  } ?>
</html>
